// Copyright (C) 2012 - Thomaz Rodrigues Botelho
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function demo_r_paralelo()

  mode(-1);
  lines(0);

  disp("r_paralelo(3,2)");
  disp(r_paralelo(3,2));

endfunction


demo_r_paralelo();
clear demo_r_paralelo;
