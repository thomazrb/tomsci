// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA - Allan CORNET
// Copyright (C) 2011 - DIGITEO - Allan CORNET
// Copyright (C) 2012 - Thomaz Rodrigues Botelho
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()
  demopath = get_absolute_file_path("tomsci.dem.gateway.sce");

  subdemolist = ["demo r_paralelo"             ,"r_paralelo.dem.sce"; ..
                 "demo r_serie"               ,"r_serie.dem.sce" ; ];

  subdemolist(:,2) = demopath + subdemolist(:,2);
  
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
