;##############################################################################################################
; Inno Setup Install script for Biblioteca Pessoal do Thomaz
; Thomaz Rodrigues Botelho
; This file is released under the 3-clause BSD license. See COPYING-BSD.
;##############################################################################################################
; modify this path where is tomsci directory
#define BinariesSourcePath "/home/thomaz/prog/scilab/tomsci"
;
#define tomsci_version "0.1"
#define CurrentYear "2012"
#define lib_circuitosDirFilename "tomsci"
;##############################################################################################################
[Setup]
; Debut Donn�es de base � renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=Biblioteca pessoal do Thomaz
AppVerName=Biblioteca pessoal do Thomaz versao 0.1
DefaultDirName={pf}\{#tomsciDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Thomaz Rodrigues Botelho
BackColorDirection=lefttoright
AppCopyright=Copyright � {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#tomsci_version}
VersionInfoCompany=Thomaz Rodrigues Botelho
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
Source: etc\tomsci.quit; DestDir: {app}\etc
Source: etc\tomsci.start; DestDir: {app}\etc
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: tests\*.*; DestDir: {app}\tests; Flags: recursesubdirs
;Source: includes\*.h; DestDir: {app}\includes; Flags: recursesubdirs
;Source: locales\*.*; DestDir: {app}\locales; Flags: recursesubdirs
Source: demos\*.*; DestDir: {app}\locales; Flags: recursesubdirs
;
;##############################################################################################################
