// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================
// load tomsci
if ~isdef('r_serie')  then
  root_tlbx_path = SCI+'\contrib\tomsci\';
  exec(root_tlbx_path + 'loader.sce',-1); 
end
//=================================
if r_serie(3,2) <> 1.2 then pause,end
//=================================
