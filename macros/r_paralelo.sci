// ====================================================================
// THOMAZ RODRIGUES BOTELHO - 2012
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
//
//
function [r_equivalente]=r_paralelo(r1, r2)
  r_equivalente = r1*r2/(r1+r2);
endfunction
// ====================================================================
